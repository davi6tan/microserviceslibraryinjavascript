
# Selected Javascript Library

 - for microservices - Seneca
 
 - for Functional Reactive Programming - Bacon Js
 
 - for Mysql schema - Sequelize
 
 - for bootstrap UI - Bootstrap 4


*Adapted from* [modern javascript](https://www.packtpub.com/web-development/modern-javascript-applications)